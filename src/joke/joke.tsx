import * as React from "react";
import './joke.scss'

export interface Props {
    text: string
    onVerdict : (verdict: boolean, index : number) => void;
}

export interface State {
}

export default class Joke extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props)

        this.state = {
        }
    }

    render() {
        return ( 
            <div className="App-link">
                {this.props.text}
            </div>  
        );

    }
}
