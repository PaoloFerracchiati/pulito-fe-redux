import { createStore, Store, applyMiddleware } from 'redux'
import rootReducer from '../reducers'
import logger from 'redux-logger';
import createSagaMiddleware from '@redux-saga/core';
import { composeWithDevTools } from 'redux-devtools-extension'
import rootSaga from '../sagas';

const sagaMiddleware = createSagaMiddleware();



const store: Store = createStore(rootReducer, composeWithDevTools(applyMiddleware(sagaMiddleware),applyMiddleware(logger)));

sagaMiddleware.run(rootSaga);

export default store

