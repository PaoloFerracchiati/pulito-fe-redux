import { jokeWatcherSaga } from "./joke";
import { all } from "redux-saga/effects";

export default function* rootSaga(){
    yield all([
        jokeWatcherSaga()
    ])
}