import { takeLatest, call, put } from 'redux-saga/effects';
import axios from 'axios'

export function* jokeWatcherSaga(){
    yield takeLatest("GET_JOKE_REQUEST", jokeWorkerSaga);
}

function fetchJoke() {
    return axios.get('https://api.chucknorris.io/jokes/random');
}

function* jokeWorkerSaga(){
    try {
        const response = yield call(fetchJoke);
        const joke = response.data;
        yield put({ type: "GET_JOKE_SUCCESS", payload : joke });
      } catch (error) {
        yield put({ type: "GET_JOKE_FAILURE", error });
      }
}