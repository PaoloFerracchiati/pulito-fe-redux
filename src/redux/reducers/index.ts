import { combineReducers } from "redux";
import jokes from './joke'
import jokeApi from './joke-api'

export interface RootState {
    jokes : string[],
    jokeApi : any
}

 export default combineReducers({
     jokes,
     jokeApi
 })