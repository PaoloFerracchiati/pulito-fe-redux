import { ApiAction } from "../actions";

export const GET_JOKE_REQUEST = 'GET_JOKE_REQUEST';
export const GET_JOKE_SUCCESS = 'GET_JOKE_SUCCESS';
export const GET_JOKE_FAILURE = 'GET_JOKE_FAILURE';


const initialState = {
  fetching: false,
  joke: null,
  error: null
};

export default function reducer(state = initialState, action: ApiAction) {
  switch (action.type) {
    case GET_JOKE_REQUEST:
      return { ...state, fetching: true, error: null , joke : null};
    case GET_JOKE_SUCCESS:
      return { ...state, fetching: false, joke: action.payload };
    case GET_JOKE_FAILURE:
      return { ...state, fetching: false, joke: null, error: action.error };
    default:
      return state;
  }
}
