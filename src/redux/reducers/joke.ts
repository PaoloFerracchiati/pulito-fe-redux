export const LIKE = 'LIKE';

export const like = (text: string) => {
    return {
        type : LIKE,
        payload : text
    }
}

const initialState: string[] = [];

export default (state:string[] = initialState, action: any )  => {
    switch (action.type){
        case LIKE : return [action.payload , ...state];
        default : return state;
    }
}
