import { Action } from "redux";

export interface AppAction extends Action{
    payload: any
}

export interface ApiAction extends AppAction{
    fetching: boolean
    error : any
}