import React from 'react';
import logo from './../assets/logo.svg';
import './app.scss';
import './../assets/animate.css'
import Joke from '../joke';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsUp, faThumbsDown } from '@fortawesome/free-regular-svg-icons';
import { connect } from 'react-redux';
import { like } from '../redux/reducers/joke';
import { RootState } from '../redux/reducers';

export interface Props {
    like : (text: string) => void
    onRequestJoke : () => void
    jokes : string[]
    fetching : boolean
    joke : any
    error : any
}

export interface State {
    liked : number,
    disliked : number,
    clicked : number,
}

const mapStateToProps = (state : RootState) => {
    return {
        jokes : state.jokes,
        fetching : state.jokeApi.fetching || false,
        joke : state.jokeApi.joke || {},
        error : state.jokeApi.error || {}
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        like : (text: string) => {
            dispatch(like(text))
        },
        onRequestJoke : () => dispatch ({type : "GET_JOKE_REQUEST"})
    }
}

class App extends React.Component < Props,State > {

    constructor(props : Props) {
        super(props);
        this.state = {
            liked : 0,
            disliked : 0,
            clicked : 0,
        }
    }

    componentDidMount(){
        this.getJoke()
    }

    getJoke(){
        this.props.onRequestJoke();
    }

    onVerdict(verdict: boolean){
        if(!this.props.fetching){
            if(verdict){
                this.setState(state => ({
                    liked : state.liked+1,
                    clicked : 1,
                }))
                this.props.like(this.props.joke.value)
            }
            else{
                this.setState(state => ({
                    disliked : state.disliked+1,
                    clicked : 2,
                }))
            }
        }
        this.getJoke()
    }

    render() {
        let buttons = (
            <span className = "buttonsContainer" >
                <span onClick={() => this.onVerdict(true)} onAnimationEnd={() => this.setState({ clicked : 0})}>
                    <FontAwesomeIcon className={`green likeIcon ${this.state.clicked === 1 ? 'animated tada fast' : ''}`}  icon={faThumbsUp} size="3x"></FontAwesomeIcon>
                </span>
                <span onClick={() => this.onVerdict(false)} onAnimationEnd={() => this.setState({ clicked : 0})} >
                    <FontAwesomeIcon className={`red likeIcon ${this.state.clicked === 2 ? 'animated rubberBand fast' : ''}`} icon={faThumbsDown} size="3x"></FontAwesomeIcon>
                </span>
            </span>
        );
        let jokes: string[] = this.props.jokes.length > 0 ? this.props.jokes : [];
        return (
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <div className="App-link"><p>You liked : {this.state.liked} Jokes</p><p>You disliked : {this.state.disliked} Jokes</p></div>
                {buttons}
                {jokes.map((joke, index) => <div key={index}>{joke}</div>)}
                <Joke text={this.props.joke.value} key={this.props.joke.id} onVerdict={this.onVerdict.bind(this)}></Joke>
            </header>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);


