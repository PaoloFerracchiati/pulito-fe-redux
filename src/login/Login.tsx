import * as React from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { faFacebookSquare, faTwitterSquare, faGooglePlusSquare } from '@fortawesome/free-brands-svg-icons'
import { faKey, faUser } from '@fortawesome/free-solid-svg-icons'

export interface Props {
    children?: React.ReactNode
}

export interface State {
}

export default class Login extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props)

        this.state = {
        }
    }

    render() {
        return (
            <div className="niceBg">
                <div className="row h-100 justify-content-center align-items-center">
                    {/* <div classNameName="col-sm-4 text-center"> */}
                    {/* <img src={logo} classNameName="App-logo" alt="logo" /> */}
                    {/* </div> */}
                    <div className="card">
                        <div className="card-header">
                            <h3>Login</h3>
                            <div className="d-flex justify-content-end social_icon">
                                <span>
                                  <FontAwesomeIcon icon={faFacebookSquare}></FontAwesomeIcon>
                                </span>
                                <span>
                                  <FontAwesomeIcon icon={faGooglePlusSquare}></FontAwesomeIcon>
                                </span>
                                <span>
                                  <FontAwesomeIcon icon={faTwitterSquare}></FontAwesomeIcon>
                                </span>
                            </div>
                        </div>
                        <div className="card-body">
                            <form>
                                <div className="input-group form-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">
                                            <FontAwesomeIcon icon={faUser}></FontAwesomeIcon>
                                        </span>
                                    </div>
                                    <input type="text" className="form-control" placeholder="username"/>
                                </div>
                                <div className="input-group form-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">
                                            <FontAwesomeIcon icon={faKey}></FontAwesomeIcon>
                                        </span>
                                    </div>
                                    <input type="password" className="form-control" placeholder="password"/>
                                </div>
                                <div className="row align-items-center remember">
                                    <input type="checkbox"/>Remember Me
                                    </div>
                                    <div className="form-group">
                                        <input type="submit" value="Login" className="btn float-right login_btn"/>
                                    </div>
                                </form>
                            </div>
                            <div className="card-footer">
                                <div className="d-flex justify-content-center links">
                                    Don't have an account?<a href="#">Sign Up</a>
                                </div>
                                <div className="d-flex justify-content-center">
                                    <a href="#">Forgot your password?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
      )}
}
